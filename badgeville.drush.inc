<?php

/**
 * @file
 * Supply a drush command to delete a user from badgeville.
 */

/**
 * Implements hook_drush_help().
 */
function badgeville_drush_help($section) {
  switch ($section) {
    case 'drush:bv_delete_user':
      return dt('Delete a user from badgeville given an email address.');
  }
}

/**
 * Implements hook_drush_comand().
 */
function badgeville_drush_command() {
  $items = array();

  $items['bv_delete_user'] = array(
    'description' => "Deletes a user from the badgeville service.",
    'arguments' => array(
      'email_address' => 'The email address of the user to delete.',
    ),
    'examples' => array(
      'drush dbvu username@example.com' => 'Delete user with email username@example.com from the badgeville service.',
    ),
    'aliases' => array('bvdu'),
  );

  return $items;
}

/**
 * Delete a user from the badgeville service.
 */
function drush_badgeville_bv_delete_user($email_address = 0) {
  global $user;

  $email_valid = 0;
  $non_drupal_email_delete_anyway = 0;

  // Confirm user passed in an email address.. needs better checking.
  if ($email_address) {
    $account = user_load_by_mail($email_address);

    // Check for email in drupal and deal with it.
    if (empty($account->mail)) {

      // Email isn't in drupal? Ask user to confirm deletion from badgville.
      drush_log(dt("$email_address not found in drupal.\n"));
      if (!drush_confirm(dt("Delete $email_address from badgeville anyway?"))) {
        if (function_exists('drush_user_abort')) {
          return drush_user_abort();
        }
        else {
          return;
        }
      }
      else {
        $non_drupal_email_delete_anyway = 1;
        $email_valid = 1;
        // Avoid clashes by storing email address in the account object.
        $account->mail = $email_address;
      }
    }
    else {
      $email_valid = 1;
    }

    // Ask user to confirm deletion, if not already confirmed non drupal email.
    if (!$non_drupal_email_delete_anyway) {
      if (!drush_confirm(dt("Are you sure you want to delete $email_address from badgeville?"))) {
        if (function_exists('drush_user_abort')) {
          return drush_user_abort();
        }
        else {
          return;
        }
      }
    }

    // If we made it this far definately delete the user from BV.
    drush_log(dt("Deleting user $email_address from badgeville.\n"));

    badgeville_user_delete($account);
  }

  if (!$email_valid) {
    drush_log(dt("Please supply a valid email address.\n"));
  }
}
