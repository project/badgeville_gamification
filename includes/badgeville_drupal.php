<?php

/**
 * @file
 * Extend the Badgville class for use in badgeville drupal module.
 * see: badgeville.class
 */

class BadgevilleDrupal extends Badgeville {

  /**
   * Configure BadgevilleDrupal.
   *
   * Gather basic information from drupal for use in the class.
   * By overriding configure() here, we can have the class use the
   * the drupal vars instead of the ini file.
   */
  public function configure($uid = NULL) {

    // If no user specified, use the logged in user.
    if (!isset($uid)) {
      global $user;
    }

    $this->networkId  = variable_get('badgeville_api_network_id');
    $this->apiPrivate = variable_get('badgeville_api_key_private');
    $this->apiPublic  = variable_get('badgeville_api_key_public');
    $domains          = variable_get('badgeville_domains', array('sandbox' => 'sandbox.v2.badgeville.com', 'production' => 'api.v2.badgeville.com'));
    $environment      = variable_get('badgeville_environment');

    $this->siteUrl    = variable_get('badgeville_site_url', 'example.com.au');
    $this->domain     = $domains[$environment];

    // Set the logging level.
    $this->logLevel   = variable_get('badgeville_log_level', 0);

    // Load the user object and store it.
    // Saves loading it multiple times in various methods.
    // If the $user var is available, load the user, otherwise load
    // passed in uid.
    if (isset($user)) {
      $this->account = user_load($user->uid, TRUE);
    }
    else {
      $this->account = user_load($uid, TRUE);
    }

  }

  /**
   * Replace Badgeville class's httpRequest with a drupal native version.
   */
  protected function httpRequest($service_uri, $data, $method = 'POST') {

    // Collect the necessary options to pass to drupal_http_request();
    $options = array(
      'method'  => $method,
      'data'    => http_build_query($data),
    );

    // Call drupal_http_request and return its result.
    $return = drupal_http_request($service_uri, $options);

    return $return;
  }

  /**
   * Query badgeville for user information.
   */
  public function getUser($options = array()) {

    if (empty($options)) {
      if ($this->account->badgeville->userId) {
        return parent::getUser(array('user_id' => $this->account->badgeville->userId));
      }
    }
    else {
      if (!empty($options['uid'])) {
        $account = user_load($options['uid']);
        if ($account->badgeville->userId) {
          return parent::getUser(array('user_id' => $this->account->badgeville->userId));
        }
      }
      else {
        return parent::getUser($options);
      }
    }

  }

  /**
   * Query badgeville for player information.
   */
  public function getPlayer($options = array()) {

    if (empty($options)) {
      if ($this->account->badgeville->playerId) {
        return parent::getPlayer(array('player_id' => $this->account->badgeville->playerId));
      }
    }
    else {
      if (!empty($options['uid'])) {
        $account = user_load($options['uid']);
        if ($account->badgeville->playerId) {
          return parent::getPlayer(array('player_id' => $this->account->badgeville->playerId));
        }
      }
      else {
        return parent::getPlayer($options);
      }
    }

  }

  /**
   * Gather basic user info from drupal, and set relevant object properties.
   */
  public function setUser() {

    // Only set user data if user is valid.
    if ($this->account->uid) {

      // Get the userId and the playerId from the $user object.
      $user_id = NULL;
      $player_id = NULL;

      // Use the realname field for the display name if available.
      // It is supplied by the realname module.
      if (module_exists('realname')) {
        $displayname = $this->account->realname;
      }

      if (!empty($this->account->badgeville->userId)) {
        $user_id = $this->account->badgeville->userId;
      }

      if (!empty($this->account->badgeville->playerId)) {
        $player_id = $this->account->badgeville->playerId;
      }

      if (empty($player_id)) {
        $player_id = db_query('SELECT uid, player_id, user_id FROM {badgeville_users} WHERE uid IN (:uids)', array(':uids' => array($this->account->uid)))->fetchObject()->player_id;
      }

      // Collect or the above retriveddata into an array.
      $bv_user_details = array(
        'firstname'   => $this->getDrupalUserFirstname(),
        'lastname'    => $this->getDrupalUserLastname(),
        'mail'        => $this->account->mail,
        'profile_pic' => $this->getDrupalUserPicLocation(),
        'user_id'     => $user_id,
        'player_id'   => $player_id,
      );

      // Assign a displayname to the user if we have one.
      if (isset($displayname)) {
        $bv_user_details['displayname'] = $displayname;
      }

      // Create a badgvillle_user and store it in this object.
      $this->user   = new BadgevilleUser($bv_user_details);

      $user_data    = new stdClass();
      $player_data  = new stdClass();

      /*
       * Detect if this user AND player has been registered with
       * badgeville and their details cached and if not, do it now.
       * Although badgeville distinguishes between users and players,
       * we don't.. so create both.
       */
      if (!$this->user->playerId() && (user_is_logged_in() && !in_array('unauthenticated user', $this->account->roles) || drupal_is_cli())) {

        /*
         * Create a new user in badgeville, whether we get a failure because the
         * user already exists or not.. either way, go get the lastest data. Yes
         * data about the created user is returned when a user is created, but
         * its a different format than that returned by querying the user,
         * query the user ensures we always have the same object structure in
         * $user_res
         */
        $user_res = $this->createUser();
        $user_res = $this->userData($this->user->mail());

        // Now do the same for the player.
        $player_res = $this->createPlayer();
        $player_res = $this->playerData($this->user->mail());

        /*
         *  Upon success for badgville registration of playerId and userId,
         *  cache user details and reload the user data into the class.
         */

        // Check if both user and player exist.
        if (in_array($user_res->code, $this->httpCodes['success']) && in_array($player_res->code, $this->httpCodes['success'])) {

          $user_data    = json_decode($user_res->data);
          $player_data  = json_decode($player_res->data);

          // Cache the userId and playerId in drupal.
          db_insert('badgeville_users')
            ->fields(array(
              'uid'       => $this->account->uid,
              'user_id'   => $user_data->data->_id,
              'player_id' => $player_data->data->_id,
            ))
            ->execute();

          /*
           * So we just got user and player id's for this user from badgville..
           * make sure the current local user object has them.
           */
          $bv_user_details = array(
            'firstname'   => $this->getDrupalUserFirstname(),
            'lastname'    => $this->getDrupalUserLastname(),
            'mail'        => $this->account->mail,
            'profile_pic' => image_style_url('thumbnail', $this->getDrupalUserPicLocation()),
            'user_id'     => $user_data->data->_id,
            'player_id'   => $player_data->data->id,
          );

          // Assign a displayname to the user if we have one.
          if (isset($displayname)) {
            $bv_user_details['displayname'] = $displayname;
          }

        }
      }
    }
    parent::setUser($bv_user_details);
  }

  /**
   * Retrieve the user's first name from drupal.
   *
   * this method retrieves the firstname from a default location
   * of a field in the user object called field_firstname.
   * if you want the firstname from a different location then
   * override this method in an extended class.
   */
  protected function getdrupaluserfirstname() {
    // Get values of user's firstname and lastname.
    $firstname_items  = field_get_items('user', $this->account, 'field_firstname');
    $firstname        = render(field_view_value('user', $this->account, 'field_firstname', $firstname_items[0]));
    return ($firstname);
  }

  /**
   * Retrieve the user's lastname from drupal.
   *
   * This method retrieves the lastname from a default location
   * of a field in the user object called field_firstname.
   * If you want the firstname from a different location then
   * override this method in an extended class.
   */
  protected function getDrupalUserLastname() {
    $lastname_items   = field_get_items('user', $this->account, 'field_lastname');
    $lastname         = render(field_view_value('user', $this->account, 'field_lastname', $lastname_items[0]));
    return ($lastname);
  }

  /**
   * Retrieve the user's picture location.
   *
   * This method retrieves the picture location from a default location
   * of a field in the user object called field_firstname.
   * If you want the firstname from a different location then
   * override this method in an extended class.
   */
  protected function getDrupalUserPicLocation() {
    $pic_uri = '';
    if (is_object($this->account->picture)) {
      $pic_uri = file_create_url($this->account->picture->uri);
    }
    else {
      $pic_uri = variable_get('user_picture_default', '');
    }
    return ($pic_uri);
  }

  /**
   * Get all the reward definitions from badgeville.
   */
  public function rewardDefinitionsData() {

    if ($data = cache_get('bv_reward_definitions')) {
      return $data->data;
    }
    elseif ($data = parent::rewardDefinitionsData()) {
      cache_set('bv_reward_definitions', $data);
      return $data;
    }
    return FALSE;
  }

  /**
   * Get all the reward definitions from badgeville.
   */
  public function activityDefinitionsData() {

    if ($data = cache_get('bv_activity_definitions')) {
      return $data->data;
    }
    elseif ($data = parent::activityDefinitionsData()) {
      cache_set('bv_activity_definitions', $data);
      return $data;
    }
    return FALSE;
  }

  /**
   * Send an http request and log it.
   */
  public function httpRequestLogger($service_uri, $data = array(), $method = 'GET') {
    global $user;

    $ret = parent::httpRequestLogger($service_uri, $data, $method);

    $trace = debug_backtrace();
    $caller = $trace[1];

    if (!empty($ret->data)) {
      $response = $ret->data;
    }
    else {
      $response = '';
    }

    db_insert('badgeville_log')
    ->fields(array(
      'calling_method' => $caller['function'],
      'logged_in_uid' => $user->uid,
      'badgeville_uid' => $this->account->uid,
      'user_id' => $this->user->userId(),
      'player_id' => $this->user->playerId(),
      'email' => $this->user->mail(),
      'api_end_point' => $service_uri,
      'sent_data' => http_build_query($data),
      'send_method' => $method,
      'response_code' => $ret->code,
      'response' => $response,
      'created_date' => REQUEST_TIME,
    ))
    ->execute();

  }

}
