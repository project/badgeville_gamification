#!/usr/bin/php
<?php

/**
 * @file
 * Check service status of the badgville server
 */


require_once 'badgeville.php';

$badgeville = new Badgeville();
$badgeville->log_level = 0;

// Get the site data.
$site_res = $badgeville->siteData();

if (!is_object($site_res)) {
  echo "CRITICAL: failed to retreive data object from badgeville\n";
  exit(2);
}

switch ($site_res->code) {
  case -110:
    echo "CRITICAL: failed connecting to badgeville - connection timed out\n";
    exit(2);
    break;

  case 400:
    echo "CRITICAL: failed retrieving data from badgeville - bad request\n";
    exit(2);
    break;
}

// If sucessful response, check for site_id.
if (in_array($site_res->code, $badgeville->httpCodes['success'])) {
  $site_data = json_decode($site_res->data);
}

if (!empty($site_data->data->_id)) {
  echo "OK: retrieved site id from badgeville\n";
}
else {
  echo "CRITICAL: did not retrieve site id from badgeville\n";
  exit(2);
}
