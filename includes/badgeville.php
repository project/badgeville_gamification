<?php

/**
 * @file
 * A PHP wrapper for the API for the badgevilel gamification service.
 * The goal of this class is to simplify the process of working with
 * badgvilles standard API, providing methods to return needed javascript
 * scripts, do HTTP requests and ease the standard API calls.
 *
 * It can run as a native PHP class, getting its config from a
 * badgeville.ini file also by extending this class, and overriding
 * configure() and setUser() (even httpRequest if desired).
 * It can itegrate seemlessly with existing systems (eg: drupal).
 */

class Badgeville {
  protected $urlCommonCurl;
  protected $urlCommonJs;
  protected $apiPrivate;
  protected $apiPublic;
  protected $domain;
  protected $siteUrl;
  protected $autoRewards;
  protected $siteData;

  // Store the user data as badgeville retrieved from badgeville.
  protected $bvUserData;

  // Store the player data as badgeville retrieved from badgeville.
  protected $bvPlayerData;

  public $httpCodes;

  // Set loggin level.
  // 0 == No error loging.
  // 1 == Log everything.
  // 2 == Only log failures.
  public $logLevel;

  public $user;

  /**
   * Constructor method.
   *
   * Upon instantiation, collect or the necessary options and data.
   */
  public function __construct($uid = NULL) {
    // Define the httpCodes lookup array.
    $this->httpCodes();
    // Setup basic properties.
    $this->configure($uid);
    // Set up the common url.
    // Do this before set user setUser may use a user in badgeville.
    $this->urlCommonCurl();
    // Define in an extended class.
    $this->setUser();
    // Turn on full error logging.
    $this->log_level = 1;
    $this->autoRewards = array();

    $this->siteData = json_decode($this->siteData()->data)->data;
  }


  /**
   * Configuration method used for standalone mode.
   *
   * Reads configutation options from badgeville.ini
   * and set the relevent properties.
   */
  protected function configure($uid = NULL) {

    // Load settings from badgeville.ini.
    $settings = parse_ini_file('badgeville.ini', TRUE);

    // Store relevant settings in appropriate properties.
    // Store the private API key.
    $this->apiPrivate = $settings['api_keys']['private'];

    // Store the public API key.
    $this->apiPublic = $settings['api_keys']['public'];

    // Store the environment we wish to work in (sandbox or production).
    $this->domain = $settings['domains'][$settings['environment']];

    // Store the site_url (id of the 'site' in badgeville).
    $this->siteUrl = $settings['site_url'];

  }

  /**
   * Map HTTP codes to their textual status.
   *
   * Provide and array of HTTP  codes mapped to their textual description
   * and split into SUCESS codes and ERROR codes.
   */
  protected function httpCodes() {

    if (empty($this->httpCodes)) {
      $this->httpCodes = array(
        'success' => array(
          'OK'      => 200,
          'Created' => 202,
        ),
        'failure' => array(
          'Bad Request'           => 400,
          'Unprocessable entity'  => 403,
          'Permission denied'     => 403,
          'Not Found'             => 404,
          'Invalid object'        => 422,
          'Internal Server Error' => 500,
        ),
      );
    }

  }

  /**
   * Query badgeville for user information.
   */
  public function getUser($options = array()) {
    if (empty($options)) {
      $service_uri = "$this->urlCommonCurl/users/{$this->user->userId}.json";
      return json_decode($this->httpRequestLogger($service_uri)->data)->data;
    }
    elseif (!empty($options['user_id'])) {
      $service_uri = "$this->urlCommonCurl/users/{$options['user_id']}.json";
      return json_decode($this->httpRequestLogger($service_uri)->data)->data;
    }
    elseif (!empty($options['email'])) {
      $service_uri = "$this->urlCommonCurl/users/{$options['email']}.json";
      return json_decode($this->httpRequestLogger($service_uri)->data)->data;
    }
  }

  /**
   * Query badgeville for player information.
   */
  public function getPlayer($options = array()) {
    if (empty($options)) {
      $service_uri = "$this->urlCommonCurl/players/{$this->user->playerId}.json";
      return json_decode($this->httpRequestLogger($service_uri)->data)->data;
    }
    elseif (!empty($options['player_id'])) {
      $service_uri = "$this->urlCommonCurl/players/{$options['player_id']}.json";
      return json_decode($this->httpRequestLogger($service_uri)->data)->data;
    }
    elseif (!empty($options['email'])) {
      $service_uri = "$this->urlCommonCurl/players/{$options['email']}.json";
      return json_decode($this->httpRequestLogger($service_uri)->data)->data;
    }
  }

  /**
   * Instantiate a badgeville_user object and store it.
   *
   * Creates an emptbadgeville user object and stores it.
   * We need a badgeville user object to operate, but often this
   * is instatiated before a user is available.
   */
  public function setUser($bv_user_details = NULL) {
    // No user needed for what we are doing, create a dummy one.
    if (empty($bv_user_details)) {
      $this->user = new BadgevilleUser(
        array(
          'firstname' => NULL,
          'lastname' => NULL,
          'mail' => NULL,
          'profile_pic' => NULL,
          'player_id' => NULL,
          'user_id' => NULL,
          'displayname' => NULL,
        )
      );
    }
    else {
      $this->user = new BadgevilleUser($bv_user_details);
      $this->bvUserData   = $this->getUser();
      $this->bvPlayerData = $this->getPlayer();
    }
  }

  /**
   * Returns the protected networkId.
   */
  public function networkId() {
    return $this->networkId;
  }

  /**
   * Returns the URL of the badgeeville REST API.
   */
  public function siteUrl() {
    return $this->siteUrl;
  }

  /**
   * Returns the URL of the badgeeville REST API.
   */
  public function urlCommonCurl() {
    if (empty($this->urlCommonCurl)) {
      $this->urlCommonCurl = "http://$this->domain/api/berlin/$this->apiPrivate";
    }
    return $this->urlCommonCurl;
  }

  /**
   * Returns the URL of the badgeville Javascript API.
   *
   * The output of this method is intended to be displayed
   * within <script src=""> tags in your HTML file.
   * It pulls together the badgeville services domain name,
   * public API and the site_id and fomulates the URL.
   */
  public function urlCommonJs() {
    if (empty($this->urlCommonJs)) {
      $this->urlCommonJs = "http://$this->domain/v3/badgeville-current.js?key=$this->apiPublic&domain=$this->siteUrl";
    }
    return $this->urlCommonJs;
  }

  /**
   * Returns all the require inline scripts for display on a page.
   *
   * The output of this method is intended to be displayed
   * within <script src=""> tags in your HTML file.
   * It pulls together all the inline scripts that are set to
   * run for the badgeville API on this page request.
   */
  public function inlineJs() {

    // Collect inline scripts into a single collection.
    $scripts = '';

    // Set the user.
    $user_js = $this->userJs();
    if ($user_js) {
      $scripts .= $user_js . "\n";
    }

    // Trigger any set autorewards.
    $auto_rewards_js = $this->autoRewardsJs();
    if ($auto_rewards_js) {
      $scripts .= $auto_rewards_js . "\n";
    }

    // Insert Badgeville.ready({}) block if it contains anything.
    $ready_js = $this->readyJs();
    if ($ready_js) {
      $scripts .= $ready_js . "\n";
    }

    return $scripts;

  }

  /**
   * Returns javascript to register user with the javascript API.
   */
  public function userJs() {
    $script = 'Badgeville.setPlayer(' . $this->userDataJSon() . ');';
    return $script;
  }

  /**
   * Returns badgeville playerdata has user data.
   */
  public function userDataJson() {
    $json = '{ "email": "' . $this->user->mail() . '", "display_name": "' . $this->user->displayName() . '", "first_name": "' . $this->user->firstname() . '", "last_name": "' . $this->user->lastname() . '", "picture_url": "' . $this->user->profilePic() . '" }';
    return $json;
  }

  /**
   * Suggest readyScripts to be displayed in the Badgeville.ready({}) block.
   *
   * These scripts will be included in the return of $this->badgevilleReadyJs().
   */
  public function addReadyScript($script) {
    $this->readyScripts[] = $script;
  }

  /**
   * Returns Badgville.ready() function fille with javascript.
   */
  public function readyJs($clear = 1) {

    if (!empty($this->readyScripts)) {

      // Generate badgeville.ready style autorewards javascript.
      $ready_scripts = '';
      foreach ($this->readyScripts as $script) {
        $ready_scripts .= "\t$script\n";
      }
      if (!empty($ready_scripts)) {
        $script = "Badgeville.ready( function () {\n$ready_scripts} );";
      }

      // Clear the Badgeville.ready array.
      if ($clear) {
        $this->readyScripts = array();
      }

      return $script;
    }
    else {
      return 0;
    }
  }

  /**
   * Suggest another autoreward to be triggered by badgeville.
   *
   * These rewards will be included in the return of $this->autoRewardsJs().
   */
  public function addAutoReward($reward) {
    $this->autoRewards[] = $reward;
  }

  /**
   * Returns javascript to trigger badgeville autorewards.
   */
  public function autoRewardsJs($clear = 1) {

    if (!empty($this->autoRewards)) {

      // Generate the autorewards script.
      $script = 'Badgeville.Settings.autoReward = [\'' . implode('\',\'', $this->autoRewards) . '\'];';

      // Clear the autorewards array.
      if ($clear) {
        $this->autoRewards = array();
      }

      return $script;
    }
    else {
      return 0;
    }
  }

  /**
   * Register user's points against a behaviour in badgeville.
   */
  public function registerActivity($activity) {

    $service_uri = "$this->urlCommonCurl/activities.json";

    $data = array(
      'activity'  => array('verb' => $activity),
    );

    if ($this->user->playerId()) {
      $data['player_id'] = $this->user->playerId();
    }
    else {
      $data['site'] = $this->siteUrl;
      $data['user'] = $this->user->mail();
    }

    return $this->httpRequestLogger($service_uri, $data, 'POST');

  }

  /**
   * Register user's points against a behaviour in badgeville.
   */
  public function registerReward($reward_id) {

    $service_uri = "$this->urlCommonCurl/rewards.json";

    $data = array(
      'reward' => array(
        'definition_id'  => $reward_id,
        'site_id'  => $this->siteData->id,
        'player_id'  => $this->user->playerId(),
      ),
    );

    return $this->httpRequestLogger($service_uri, $data, 'POST');

  }

  /**
   * Register user's points against a behaviour in badgeville.
   */
  public function missionData($group_id) {
    $service_uri = "$this->urlCommonCurl/groups/$group_id.json";
    $data = array();
    return json_decode($this->httpRequestLogger($service_uri, $data, 'GET')->data)->data;
  }

  /**
   * Get all the reward definitions from badgeville.
   */
  public function rewardDefinitionsData() {

    $page = 0;
    // Badgeville maximum page size is 50, do not set this higher.
    $page_size = 50;
    $data = array();

    $complete = FALSE;
    while (!$complete) {
      $page++;
      $service_uri = "$this->urlCommonCurl/reward_definitions.json?site=$this->siteUrl&per_page=$page_size&page=$page";

      $response = $this->httpRequestLogger($service_uri, array(), 'GET');
      // If we got a failure response, return false.
      if (!in_array($response->code, $this->httpCodes['success'])) {
        return FALSE;
      }
      $result = json_decode($response->data, TRUE);
      foreach ($result['data'] as $key => $value) {
        $result['data'][$value['id']] = $value;
        unset($result['data'][$key]);
      }
      $data += $result['data'];

      if (count($result['data']) == 0 || count($result['data']) < $page_size) {
        $complete = TRUE;
      }
    }

    return $data;
  }

  /**
   * Get all the activity definitions from badgeville.
   */
  public function activityDefinitionsData() {

    $page = 0;
    // Badgeville maximum page size is 50, do not set this higher.
    $page_size = 50;
    $data = array();

    $complete = FALSE;
    while (!$complete) {
      $page++;
      $service_uri = "$this->urlCommonCurl/activity_definitions.json?site=$this->siteUrl&per_page=$page_size&page=$page";

      $response = $this->httpRequestLogger($service_uri, array(), 'GET');
      // If we got a failure response, return false.
      if (!in_array($response->code, $this->httpCodes['success'])) {
        return FALSE;
      }
      $result = json_decode($response->data, TRUE);
      $data += $result['data'];

      if (count($result['data']) == 0 || count($result['data']) < $page_size) {
        $complete = TRUE;
      }
    }

    return $data;
  }

  /**
   *  Retrieve information from badgeville about the current site.
   */
  public function siteData() {
    $service_uri = "{$this->urlCommonCurl}/sites/{$this->siteUrl}.json";
    return $this->httpRequestLogger($service_uri);
  }

  /**
   *  Create a user in badgeville.
   *
   *  Create a user in badgveille based on the currently loaded user data.
   */
  public function createUser() {

    $service_uri = "{$this->urlCommonCurl}/users.json";
    $data = array(
      'user'  => array(
        'network_id'   => $this->networkId(),
        'name'         => $this->user->displayName(),
        'email'        => $this->user->mail(),
      ),
    );

    $res = array();

    $res = $this->httpRequestLogger($service_uri, $data, 'POST');

    return $res;

  }

  /**
   *  Create a player in badgeville.
   *
   *  Create a player in badgveille based on the currently loaded player data.
   */
  protected function createPlayer() {

    $service_uri = "$this->urlCommonCurl/players.json";

    $data = array(
      'site'    => $this->siteUrl,
      'email'   => $this->user->mail(),
      'player'  => array(
        'email'         => $this->user->mail(),
        'displayName'  => $this->user->displayName(),
        'first_name'    => $this->user->firstname(),
        'last_name'     => $this->user->lastname(),
        'picture_url'   => $this->user->profilePic(),
      ),
    );

    return $this->httpRequestLogger($service_uri, $data, 'POST');

  }

  /**
   *  Delete a user from badgeville.
   */
  public function deleteUser($account) {

    // No BV UserID? Try and use email.
    if (empty($account->badgeville->userId)) {
      $email = urlencode($account->mail);
      $service_uri = "{$this->urlCommonCurl}/users/users.json?site={$this->siteUrl}&email={$email}";
    }
    else {
      $service_uri = "$this->urlCommonCurl/users/{$account->badgeville->userId}.json";
    }

    $data = array();
    return $this->httpRequestLogger($service_uri, $data, 'DELETE');

  }

  /**
   *  Delete a player from badgeville.
   */
  public function deletePlayer($account) {
    // No BV UserID? Try and use email.
    if (empty($account->badgeville->userId)) {
      $email = urlencode($account->mail);
      $service_uri = "{$this->urlCommonCurl}/users/players.json?site={$this->siteUrl}&email={$email}";
    }
    else {
      $service_uri = "$this->urlCommonCurl/players/{$account->badgeville->playerId}.json";
    }
    $data = array();
    return $this->httpRequestLogger($service_uri, $data, 'DELETE');
  }

  /**
   * Send an HTTP request.
   */
  protected function httpRequest($url, $headers = array(), $method = 'GET', $data = NULL, $retry = 3) {
    global $db_prefix;

    $result = new stdClass();

    // Parse the URL and make sure we can handle the schema.
    $uri = parse_url($url);

    if ($uri == FALSE) {
      $result->error = 'unable to parse URL';
      $result->code = -1001;
      return $result;
    }

    if (!isset($uri['scheme'])) {
      $result->error = 'missing schema';
      $result->code = -1002;
      return $result;
    }

    switch ($uri['scheme']) {
      case 'http':
      case 'feed':
        $port = isset($uri['port']) ? $uri['port'] : 80;
        $host = $uri['host'] . ($port != 80 ? ':' . $port : '');
        $fp = @fsockopen($uri['host'], $port, $errno, $errstr, 15);
        break;

      case 'https':
        // Note: Only works for PHP 4.3 compiled with OpenSSL.
        $port = isset($uri['port']) ? $uri['port'] : 443;
        $host = $uri['host'] . ($port != 443 ? ':' . $port : '');
        $fp = @fsockopen('ssl://' . $uri['host'], $port, $errno, $errstr, 20);
        break;

      default:
        $result->error = 'invalid schema ' . $uri['scheme'];
        $result->code = -1003;
        return $result;
    }

    // Make sure the socket opened properly.
    if (!$fp) {
      // When a network error occurs, we use a negative number so it does not
      // clash with the HTTP status codes.
      $result->code = -$errno;
      $result->error = trim($errstr);

      // Log that the request failed.
      error_log('http_request_failed');

      return $result;
    }

    // Construct the path to act on.
    $path = isset($uri['path']) ? $uri['path'] : '/';
    if (isset($uri['query'])) {
      $path .= '?' . $uri['query'];
    }

    // Create HTTP request.
    $defaults = array(
      /*
       * RFC 2616:"non-standard ports MUST, default ports MAY be included".
       * We don't add the port to prevent from breaking rewrite rules
       * checking the host that do not take into account the port number.
       */
      'Host' => "Host: $host",
      'User-Agent' => 'User-Agent: Drupal (+http://drupal.org/)',
    );

    /*
     * Only add Content-Length if we actually have any content or if it is
     * a POST * or PUT request. Some non-standard servers get confused by
     * Content-Length in at least HEAD/GET requests, and Squid always
     * requires Content-Length in POST/PUT requests.
     */
    $content_length = strlen($data);
    if ($content_length > 0 || $method == 'POST' || $method == 'PUT') {
      $defaults['Content-Length'] = 'Content-Length: ' . $content_length;
    }

    // If the server url has a user then attempt to use basic authentication.
    if (isset($uri['user'])) {
      $defaults['Authorization'] = 'Authorization: Basic ' . base64_encode($uri['user'] . (!empty($uri['pass']) ? ":" . $uri['pass'] : ''));
    }

    /*
     * If the database prefix is being used by SimpleTest to run the tests in
     * a copied. database then set the user-agent header to the database
     * prefix so that any calls to other Drupal pages will run the SimpleTest
     * prefixed database. The user-agent is used to ensure that multiple
     * testing sessions running at the same time won't interfere with each
     * other as they would if the database prefix were stored statically in
     * a file or database variable.
     */
    if (is_string($db_prefix) && preg_match("/^simpletest\d+$/", $db_prefix, $matches)) {
      $defaults['User-Agent'] = 'User-Agent: ' . $matches[0];
    }

    foreach ($headers as $header => $value) {
      $defaults[$header] = $header . ': ' . $value;
    }

    $request = $method . ' ' . $path . " HTTP/1.0\r\n";
    $request .= implode("\r\n", $defaults);
    $request .= "\r\n\r\n";
    $request .= $data;

    $result->request = $request;

    fwrite($fp, $request);

    // Fetch response.
    $response = '';
    while (!feof($fp) && $chunk = fread($fp, 1024)) {
      $response .= $chunk;
    }
    fclose($fp);

    // Parse response.
    list($split, $result->data) = explode("\r\n\r\n", $response, 2);
    $split = preg_split("/\r\n|\n|\r/", $split);

    list($protocol, $code, $status_message) = explode(' ', trim(array_shift($split)), 3);
    $result->protocol = $protocol;
    $result->status_message = $status_message;

    $result->headers = array();

    // Parse headers.
    while ($line = trim(array_shift($split))) {
      list($header, $value) = explode(':', $line, 2);
      if (isset($result->headers[$header]) && $header == 'Set-Cookie') {
        // RFC 2109: the Set-Cookie response header comprises the token Set-
        // Cookie:, followed by a comma-separated list of one or more cookies.
        $result->headers[$header] .= ',' . trim($value);
      }
      else {
        $result->headers[$header] = trim($value);
      }
    }

    $responses = array(
      100 => 'Continue',
      101 => 'Switching Protocols',
      200 => 'OK',
      201 => 'Created',
      202 => 'Accepted',
      203 => 'Non-Authoritative Information',
      204 => 'No Content',
      205 => 'Reset Content',
      206 => 'Partial Content',
      300 => 'Multiple Choices',
      301 => 'Moved Permanently',
      302 => 'Found',
      303 => 'See Other',
      304 => 'Not Modified',
      305 => 'Use Proxy',
      307 => 'Temporary Redirect',
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Payment Required',
      403 => 'Forbidden',
      404 => 'Not Found',
      405 => 'Method Not Allowed',
      406 => 'Not Acceptable',
      407 => 'Proxy Authentication Required',
      408 => 'Request Time-out',
      409 => 'Conflict',
      410 => 'Gone',
      411 => 'Length Required',
      412 => 'Precondition Failed',
      413 => 'Request Entity Too Large',
      414 => 'Request-URI Too Large',
      415 => 'Unsupported Media Type',
      416 => 'Requested range not satisfiable',
      417 => 'Expectation Failed',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
      502 => 'Bad Gateway',
      503 => 'Service Unavailable',
      504 => 'Gateway Time-out',
      505 => 'HTTP Version not supported',
    );
    /*
     * RFC 2616 states that all unknown HTTP codes must be treated
     * the same as the base code in their class.
     */
    if (!isset($responses[$code])) {
      $code = floor($code / 100) * 100;
    }

    /*
     * Codes:
     * 200 OK.
     * 304 Not Modified.
     * 301 Moved permanently.
     * 302 Moved temporarily.
     * 307 Moved temporarily.
     */

    switch ($code) {
      case 200:
      case 304:
        break;

      case 301:
      case 302:
      case 307:
        $location = $result->headers['Location'];

        if ($retry) {
          $result = drupal_http_request($result->headers['Location'], $headers, $method, $data, --$retry);
          $result->redirect_code = $result->code;
        }
        $result->redirect_url = $location;

        break;

      default:
        $result->error = $status_message;
    }

    $result->code = $code;
    $result->error = '';
    return $result;
  }


  /**
   * Send an http request and log it.
   */
  public function httpRequestLogger($service_uri, $data = array(), $method = 'GET') {

    $log['sent']['service_uri'] = $service_uri;
    $log['sent']['method'] = $method;

    if (!empty($data)) {
      $log['sent']['service_uri'] .= '?' . http_build_query($data);
    }

    $ret = $this->httpRequest($service_uri, $data, $method);

    $log['received'] = $ret;

    $trace = debug_backtrace();
    $caller = $trace[1];

    $this->log($log, $caller['function']);

    return $ret;
  }

  /**
   * Write to the error log.
   */
  protected function log($log, $caller = NULL) {

    if ($this->logLevel == 0) {
      // Don't log anything.
      return;
    }

    if (!isset($caller)) {
      $trace  = debug_backtrace();
      $caller = $trace[1];
      $caller = $caller['function'];
    }

    if (($this->logLevel == 2 && !in_array($log['received']->code, $this->httpCodes['success'])) || $this->logLevel == 1) {

      error_log('badgeville user_id: ' . $this->user->userId());
      error_log('badgeville player_id: ' . $this->user->playerId());
      error_log('badgeville email: ' . $this->user->mail());

      // Write the send request to the error log.
      error_log("badgeville $caller > {$log['sent']['method']}  {$log['sent']['service_uri']} {$log['sent']['data']}");

      // Only try and write data to the log if we got some.
      if (isset($log['received']->data)) {
        $data = ' ' . $log['received']->data;
      }
      else {
        $data = '';
      }

      // Only try and write errors to the log if we got some.
      if (isset($log['received']->error)) {
        $error = ' ' . $log['received']->error;
      }
      else {
        $error = '';
      }

      // Write the response to the error log.
      // Errors return the error property, other results a status message.
      if (isset($log['received']->status_message)) {
        error_log("badgeville < {$log['received']->code} {$log['received']->status_message} {$log['received']->data}");
      }
      else {
        error_log("badgeville < {$log['received']->code}{$error}{$data}");
      }
    }

  }

}


class BadgevilleUser {

  protected $name = array('firstname' => NULL, 'lastname' => NULL);
  protected $mail;
  protected $profilePic;
  protected $playerId;

  /**
   * Constructor method for BadgevilleUser class.
   */
  public function __construct($user) {
    $this->name['firstname'] = $user['firstname'];
    $this->name['lastname'] = $user['lastname'];
    $this->name['displayname'] = $user['displayname'];
    $this->mail = strtolower($user['mail']);
    $this->profilePic = $user['profile_pic'];

    $this->playerId = $user['player_id'];
    $this->userId = $user['user_id'];
  }

  /**
   * Get user's firstname.
   */
  public function firstname() {
    return $this->name['firstname'];
  }

  /**
   * Get user's lastname.
   */
  public function lastname() {
    return $this->name['lastname'];
  }

  /**
   * Get user's display_name.
   *
   * Returns the user's display name.
   * If the display name is not set, it defaults to the firstname and lastname
   * concatenated by a space.
   */
  public function displayName() {
    if (empty($this->name['displayname'])) {
      $this->name['displayname'] = implode($this->name, ' ');
    }
    return $this->name['displayname'];
  }

  /**
   * Get user's email address.
   */
  public function mail() {
    return $this->mail;
  }

  /**
   * Get user's displayName.
   *
   * Returns the url of the user's profile_pic.
   */
  public function profilePic() {
    return $this->profilePic;
  }

  /**
   * Get user's user_id.
   */
  public function userId() {
    return $this->userId;
  }

  /**
   * Get user's player_id.
   */
  public function playerId() {
    return $this->playerId;
  }

  /**
   * Get the jquery code to set the user in badgeville javascript API.
   */
  public function userJs() {
    return "jQuery(document).ready( function () { Badgeville.setPlayer({ email: '$this->mail', display_name: '" . $this->displayName() . "', first_name: '{$this->name['firstname']}', last_name: '{$this->name['lastname']}', }); });";
  }

}
