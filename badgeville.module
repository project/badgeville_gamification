<?php

/**
 * @file
 * Main module file for badgeville gamification integration.
 */

/**
 * Implements hook_menu().
 */
function badgeville_menu() {
  $items = array();

  $items['admin/config/badgeville'] = array(
    'title' => 'Badgeville Configuration',
    'description' => 'Badgville Settings.',
    'position' => 'left',
    'weight' => -5,
    'page callback' => 'system_admin_menu_block_page',
    'access callback' => '_badgeville_access',
    'access arguments' => array('administer badgeville configuration',  'manually assign badgeville items'),
  );

  $items['admin/config/badgeville/badgeville'] = array(
    'title' => 'Badgeville',
    'description' => 'Configuration for badgeville module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('badgeville_form'),
    'access arguments' => array('administer badgeville configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Determine has a user access to relevant badgeville configuration pages.
 */
function _badgeville_access() {

  $perms = func_get_args();

  foreach ($perms as $perm) {
    if (user_access($perm)) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Implements hook_permission().
 *
 * Provides permission to allow access to badgeville configuration pages.
 */
function badgeville_permission() {
  return array(
    'administer badgeville configuration' => array(
      'title' => t('Administer Badgeville Configuration'),
      'description' => t('Perform administration tasks for Badgeville.'),
    ),
  );
}

/**
 * Form function, called by drupal_get_form() in current_posts_menu().
 *
 * Create the form for the badgeville configuration page.
 */
function badgeville_form($form, &$form_state) {

  // Radios to set whether drupal sends badgeville requests
  // to badgeville sandbox or production server.
  $form['badgeville_environment'] = array(
    '#type' => 'radios',
    '#title' => t('Environment'),
    '#default_value' => variable_get('badgeville_environment', 'sandbox'),
    '#options' => array('sandbox' => t('Sandbox'), 'production' => t('Production')),
  );

  // Txtbox stores the network_id for this bageville configuration.
  $form['badgeville_api_network_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Network ID'),
    '#default_value' => variable_get('badgeville_api_network_id'),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('Badgeville Network ID found on the home  page of the Develop tab in the Badgeville Publisher module'),
    '#required' => TRUE,
  );

  // Txtbox stores the public api key for this bageville configuration.
  // This is used to access badgeille's javascript API.
  $form['badgeville_api_key_public'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key Public'),
    '#default_value' => variable_get('badgeville_api_key_public'),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('Badgeville API Key Used for javascript api'),
    '#required' => TRUE,
  );

  // Txtbox stores the private api key for this bageville configuration.
  // This is used to access badgvilels REST api (via php).
  $form['badgeville_api_key_private'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key Private'),
    '#default_value' => variable_get('badgeville_api_key_private'),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('Badgeville API Key user for curl/php api'),
    '#required' => TRUE,
  );

  // Txtbox store's the site_url that identifies this site to badgeville.
  $form['badgeville_site_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Url'),
    '#default_value' => variable_get('badgeville_site_url', 'example.com.au'),
    '#size' => 40,
    '#maxlength' => 2000,
    '#description' => t('The URL/Domain eg. www.example.com of the site to which to log events in badgeville'),
    '#required' => TRUE,
  );

  // Txtbox store's the site_url that identifies this site to badgeville.
  $form['badgeville_manual_queue_run_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Manual assign queue run size'),
    '#default_value' => variable_get('badgeville_manual_queue_run_size', 50),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Set the max entries to process at time during a run.'),
  );

  // Select box to set the log level.
  // Set logging level.
  // 0 == No error logging.
  // 1 == Log everything.
  // 2 == Only log failures.
  $log_level_options = array(
    t('No error logging'),
    t('Log everything'),
    t('Only log failures'),
  );
  $form['badgeville_log_level'] = array(
    '#type' => 'select',
    '#title' => t('Log Level'),
    '#default_value' => variable_get('badgeville_log_level', 0),
    '#options' => $log_level_options,
    '#description' => t('Set the logging level for the badgeville PHP REST API.'),
  );

  $form['badgeville_header_tab_widget'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the Header Tab Widget'),
    '#default_value' => variable_get('badgeville_header_tab_widget', 0),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_preprocess_page().
 *
 * Add all the necessary javascript for badgeville.
 * -- Load the badgeville js api.
 * -- Set the badgevile player.
 * -- Trigger any autorewards that are set.
 */
function badgeville_process_page(&$variables) {
  global $user;

  // Load needed javascript for logged in and authenticated users.
  $account = user_load($user->uid);
  if (user_is_logged_in() && !in_array('unauthenticated user', $account->roles)) {
    // Load the js API.
    drupal_add_js($_SESSION['badgeville']->urlCommonJs(), 'external');

    // If any inline scripts are set, add them.
    $inline_js = $_SESSION['badgeville']->inlineJs();
    if (!empty($inline_js)) {
      drupal_add_js($inline_js, 'inline');
    }

  }

}

/**
 * Implements hook_init().
 *
 * Initialize the badgeville class and load relevant javscript.
 */
function badgeville_init() {

  global $user;

  // If enabled show the bv_headerTab widget.
  if (variable_get('badgeville_header_tab_widget', 0)) {
    // Use jQuery to add the bv_headerTab widget.
    $bv_ht_div = '<div class="bv_headerTab"></div>';
    $bv_ht_js  = 'jQuery(document).ready(function () { jQuery("body").prepend("' . addslashes($bv_ht_div) . '")} );';
    drupal_add_js($bv_ht_js, 'inline');
  }

  // Updates to the undelying badgeville code caused no such class errors.
  if (class_exists('BadgevilleDrupal')) {

    // Create a global instance of the BadgvilelDrupal instance
    // in many cases saves creating a new instance.
    if (user_is_logged_in() && !in_array('unauthenticated user', $user->roles) && empty($_SESSION['badgeville'])) {
      $_SESSION['badgeville'] = new BadgevilleDrupal();
    }
  }

  // Detect if any persistent js has been added and display it.
  if ($scripts = badgeville_add_js()) {
    unset($_SESSION['badgeville_delayed_js']);
    foreach ($scripts as $script) {
      drupal_add_js($script['js'], $script['options']);
    }
  }


}

/**
 * Implements hook_user_load().
 *
 * Append the cached badgville userId and playerId for this drupal user
 * to the $user object.
 */
function badgeville_user_load($users) {
  $result = db_query('SELECT uid, player_id, user_id FROM {badgeville_users} WHERE uid IN (:uids)', array(':uids' => array_keys($users)));
  foreach ($result as $record) {
    $users[$record->uid]->badgeville->playerId = $record->player_id;
    $users[$record->uid]->badgeville->userId = $record->user_id;
  }
}

/**
 * Implements hook_user_delete().
 *
 * Remove cached badgeville user data when a user is deleted from drupal.
 * Also delete the player from badgeville.
 */
function badgeville_user_delete($account) {

  /*
   * The account object past here, seems to not containing items appended by
   * hook_user_load.. so load them by hand;
   */
  $result = db_query("SELECT uid, player_id, user_id FROM {badgeville_users} WHERE uid = :uid", array(':uid' => $account->uid));
  foreach ($result as $record) {

    $account->badgeville->playerId = $record->player_id;
    $account->badgeville->userId = $record->user_id;
  }

  // If a badgeville account was found for this user, delete it.
  if (count($result)) {
    $badgeville = new BadgevilleDrupal();
    db_delete('badgeville_users')
        ->condition('uid', $account->uid)
        ->execute();

    $badgeville->deletePlayer($account);
  }
}

/**
 * Implements hook_block_info().
 */
function badgeville_block_info() {

  $blocks['bv_headerTab'] = array(
    'info' => t('Badgeville bv_headerTab'),
    // DRUPAL_CACHE_PER_ROLE will be assumed.
  );

  return $blocks;
}


/**
 * Implements hook_block_view().
 */
function badgeville_block_view($delta = '') {

  switch ($delta) {
    case 'bv_headerTab':
      $block = array(
        'subject' => t('badgeville headertab'),
        'content' => '<div class="bv_headerTab"></div>',
      );
      break;
  }
  return $block;
}


/**
 * Add javascript persistently across redirects.
 *
 * @see badgeville_init()
 */
function badgeville_add_js($data = NULL, $options = NULL) {

  if ($data) {
    if (!isset($_SESSION['badgeville_delayed_js'])) {
      $_SESSION['badgeville_delayed_js'] = array();
    }

    $script = array('js' => $data, 'options' => $options);

    if (!in_array($script, $_SESSION['badgeville_delayed_js'])) {
      $_SESSION['badgeville_delayed_js'][] = $script;
    }

    // Mark this page as being uncacheable.
    drupal_page_is_cacheable(FALSE);
  }

  // Javascript not set when DB connection fails.
  return isset($_SESSION['badgeville_delayed_js']) ? $_SESSION['badgeville_delayed_js'] : NULL;

}
