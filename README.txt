-- SUMMARY --

Badgeville is a gameification service. http://www.badgeville.com.
It allows you to assign rewards / goals to certain activities. Badgeville tracks
users progress towards achievements.

This module is a minimalist module for integrating Badgeville with Drupal.

It makes it easy and simple to integrate a single drupal site into badgeville.

For a full description visit the project page:
  http://drupal.org/sandbox/bevan.wishart/1309370

Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/1309370

-- REQUIREMENTS --

* An account on http://www.badgeille.com


-- INSTALLATION --

* Install as usual, see http://drupal.org/project/1309370/git-instructions for
further information.
* copy the cloned code into you /sites/all/modules/custom directory

-- CONFIGURATION --

You may configure the environment at /admin/config/content/badgeville

-- CONTACT --

Author maintainers:
* Bevan Wishart (bevan.wishart), www.catalyst-au.net


This project has been sponsored by:
* Catalyst IT Australia - Visit http://www.catalyst-au.net for more information.


== Badgeville in Drupal ==
Badgeville is integrated into drupal using two separate modules, badgeville
which automatically adds the relevant js and supplies an API that can be
accessed in your own custom module.

=== The Badgeville Module ===
This is a minimilist module for integrating Badgeville with Drupal.

Once installed, enabled and configured the badgeville module
* automatically includes badgevilles' tracker js into every page view by a
logged in user
* sets the user in that tracker to the currently logged in user.
* creates a global $badgeville object available for use in other
modules.
* in some cases (depending on program flow) the global may not yet be available
so just do:
$badgeville = new badgeville();
* Provides blocks for display of some badgeville widgets.
(currently bv_headerTab only).

See documentation for the badgeville class elsewhere on this page for usage
examples

==== Installation ====
Further info and download: http://drupal.org/sandbox/bevan.wishart/1309370

* copy the code to sites/all/modules/custom
* browse to http://[your sites domain]/admin/config/content/badgeville
* choose sandbox (unless you really want to send to badgevilles' production
server)
* copy and paste the network id, public and private api key's from
http://sandbox.v2.badgeville.com/devcenter/home into the relevant fields
* enter the url of the site you want to register events against. available sites
can be seen here: http://sandbox.v2.badgeville.com/sites

=== The badgeville Module API ===
Uses drupal's hook system to trigger various events within badgeville via the
class supplied by the badgeville module.

Examples

1. Award points to the currently logged in user for logging in
function mymodule_badgeville_user_login(&$edit, $account) {
    //make sure badgeville object is initialised with current user
    $badgeville = new badgeville_drupal();
    $res = $badgeville->register_activity('logged in');
}

2. Award points to the currently logged in user for logging in
function mymodule_badgeville_user_login(&$edit, $account) {
    //make sure badgeville object is initialised with current user
    $badgeville = new badgeville_drupal();
    $res = $badgeville->autoReward('logged in');
}

3. Append a button to a node and and register a click event via ajax (using
badgevilles tracker code include by the badgeville module
function mymodule_badgeville_node_view($node, $view_mode, $langcode) {
  global $badgeville, $user;
  if ($node->nid == 1 && user_is_logged_in()) {
    $node->content['event button'] = array(
      '#markup' => "<br /><button onclick=\"Badgeville.credit('logged in');\"> \
Give credit for reading anything.</button> <div class=\"bv_headerTab\"></div>",
    );

  }

}

== The Badgeville PHP class ==
The code for the badgeville class is currently in the includes directory of
the badgeville module which can be found in the git repository on bevan's
sandbox on drupal.org, here: http://drupal.org/sandbox/bevan.wishart/1309370

The badgeville class can work standalone in plane old PHP by putting settings in
a badgeville.ini file in the same directory as the class.
Or integrated into other systems such drupal (as seen in this module) by
extending some methods:
configure(),
httpRequest() (optional),
setUser().

class MyBadgeville extends Badgeville {
protected function configure() {
}
protected function setUser() {
}
protected function httpRequest() {
}
}

=== The configure method ===
Can be overridden to make use of of any built in settings in your cms
(eg drupal).
configure() must set the classes' api_private, api_public, site_url and domain
properties

* api_private: is the private key of the badgeville api used for the REST
service (accessed via curl or some other HTTP access method)
* api_public: is the public key of the badgeville api used to include the
javascript tracker code
* site_url: is the url of the site in badgeville that events should be
registered against
* domain: is the domain name of the badgeville server
eg. sandbox.v2.badgeville.com

Drupal Example:
  /**
   * Configure BadgevilleDrupal.
   *
   * Gather basic information from drupal for use in the class.
   * By overriding configure() here, we can have the class use the
   * the drupal vars instead of the ini file.
   */
  public function configure($uid = NULL) {

    // If no user specified, use the logged in user.
    if (!isset($uid)) {
      global $user;
    }

    $this->networkId  = variable_get('badgeville_api_network_id');
    $this->apiPrivate = variable_get('badgeville_api_key_private');
    $this->apiPublic  = variable_get('badgeville_api_key_public');

    $domain_list = array(
      'sandbox' => 'sandbox.v2.badgeville.com',
      'production' => 'api.v2.badgeville.com',
    );
    $domains          = variable_get('badgeville_domains', $domain_list);
    $environment      = variable_get('badgeville_environment');

    $this->siteUrl    = variable_get('badgeville_site_url', 'example.com.au');
    $this->domain     = $domains[$environment];

    // Set the logging level.
    $this->logLevel   = variable_get('badgeville_log_level', 0);

    // Load the user object and store it.
    // Saves loading it multiple times in various methods.
    // If the $user var is available, load the user, otherwise load
    // passed in uid.
    if (isset($user)) {
      $this->account = user_load($user->uid, TRUE);
    }
    else {
      $this->account = user_load($uid, TRUE);
    }

  }

=== The http_request method ===
The badgeville class has its own built in http_request method or can be extended
to make use of any built in http_request methods in your cms and return its
result.

drupal example:
  protected function httpRequest($service_uri, $data, $method = 'POST') {
    $options = array(
        'headers' => array('Content-Type' =>  \
'application/x-www-form-urlencoded; charset=utf-8'),
        'method' => $method,
        'data' => http_build_query($data),
        );

    return drupal_http_request($service_uri, $options);
  }

=== The set_user method ===
must set the classes user property to an instantiated badgeville_user object.

Plain php example:
  protected function setUser() {
     $bv_user_details = array(
       'firstname' => 'John',
       'lastname' => 'Smith',
       'mail' => 'john.smith@example.com',
       'profile_pic' => 'http://path_to_some_image/image.png',
       'player_id' => '1234',
     );

     $this->user = new BadgevilleUser($bv_user_details);
   }

=== Simple php usage example: ===

require_once('includes/badgeville.php');

class MyBadgeville extends Badgeville {
  protected function configure() {

    $this->api_private = '13243124132';
    $this->api_public = '98732498794';
    $this->site_url = 'd.plane.edu.au'
    $this->domain = 'sandbox.v2.badgeville.com'

  }

  protected function http_request($service_uri, $data, $method = 'POST') {
   ...
   //some code to implement a http_request
   ...
   return $result;
  }

  protected function setUser() {
        $bv_user_details = array(
          'firstname' => 'Bevan',
          'lastname' => 'Wishart',
          'mail' => 'bevan@catalyst-au.net',
          'profile_pic' => 'http://path_to_some_image/image.png',
          'player_id' => '1234',
          );

      $this->user = new BadgevilleUser($bv_user_details);
    }

  }

}

$badgeville = new MyBadgeville();
