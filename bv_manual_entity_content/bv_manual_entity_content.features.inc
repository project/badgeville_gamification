<?php
/**
 * @file
 * bv_manual_entity_content.features.inc
 */

/**
 * Implements hook_node_info().
 */
function bv_manual_entity_content_node_info() {
  $items = array(
    'manual_bv_entity' => array(
      'name' => t('Manually Assignable Badgeville Entity'),
      'base' => 'node_content',
      'description' => t('Describe a badgeville element such as a behaivor/activity, mission, reward, track etc as defined in badgeville so it can be manually allocated via Drupal.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
